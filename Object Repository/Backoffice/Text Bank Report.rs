<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text Bank Report</name>
   <tag></tag>
   <elementGuidId>dd5eedca-1a0b-4314-bc38-6e35034411f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(.,&quot;Bank Report&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Backoffice/Iframe</value>
   </webElementProperties>
</WebElementEntity>
