//========================BASE-PATH-UNTUK-QA=====================================
/*

	Variable tempat menyimpan keseluruhan test case.

*/
var base_path = ""; 


/*

	Variable tempat url Test Case Processor.

*/ 
var base_url = "http://localhost:35001/";
//==================================================================================


/*

	Variable tempat wsdlURL.

*/ 
var wsdlURL = "http://localhost:5050";
//==================================================================================

//==================================================================================
saveVariable('base_path', base_path);
saveVariable('wsdlURL', wsdlURL);


var fileProp = storedVars['base_path'] + "user-extension\\selenium.properties";
var textProp = readTextFile2(fileProp);

var map = new Object();

function readTextFile2(locator) {
	finalLocator = "file://" + locator;
	var allText;
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", finalLocator, true);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				textProp = rawFile.responseText;
						
				var strDelimiter = "=";
				textProp.replace('\r', '');
				var tmp = textProp.split(' ');
				textProp = tmp.join('');
				var lines = textProp.split("\n");
				
				for (var i = 0; i < lines.length; i++) {
					var currentline = lines[i].replace('\r', '').split(strDelimiter);
					saveVariable(currentline[0], currentline[1]);
					map[currentline[0]] = currentline[1];
				}
				//base_url = map["base_url"];
			}
		}
	}
	rawFile.send(null);
}

Selenium.prototype.doReadProperties = function() {
//	base_url = map["base_url"];
	
	var keys = Object.keys(map);
	for (i=0;i<keys.length;i++){
		saveVariable(keys[i], map[keys[i]]);
	}
}


Selenium.prototype.doStoreTextFile = function(locator, attribute) {
	saveVariable(attribute, readTextFile(locator));
}

function readTextFile(locator) {
	finalLocator = "file://" + locator;
	var allText;
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", finalLocator, false);
	rawFile.onreadystatechange = function() {
		if (rawFile.readyState === 4) {
			if (rawFile.status === 200 || rawFile.status == 0) {
				allText = rawFile.responseText;
			}
		}
	}
	rawFile.send(null);
	return allText;
}

// convert csv to json object
function csvToJSON(strData, strDelimiter) {
	var strDelimiter = (strDelimiter || ",");
	strData.replace('\r', '');
	var lines = strData.split("\n");
	var result = [];
	result.push(obj);
	var headers = lines[0].split(strDelimiter);
	for (var i = 1; i < lines.length; i++) {
		var obj = {};
		var currentline = lines[i].split(strDelimiter);
		for (var j = 0; j < headers.length; j++) {
			obj[headers[j]] = currentline[j];
		}
		result.push(obj);
	}
	// return result; //JavaScript object
	return result; // JSON
}

// convert csv to json object
function csvToJSONProp(strData, strDelimiter) {
	var strDelimiter = (strDelimiter || ",");
	strData.replace('\r', '');
	var tmp = strData.split(' ');
	strData = tmp.join('');
	var lines = strData.split("\n");
	var resultString = "{";
	for (var i = 0; i < lines.length; i++) {
		var obj = {};
		var currentline = lines[i].replace('\r', '').split(strDelimiter);
		resultString += "\"" + currentline[0] + "\":" + "\"" + currentline[1]
				+ "\"";
		if (i < lines.length - 1) {
			resultString += ",";
		}
	}
	resultString += "}";
	return resultString; // string
}

// convert csv to array object
function csvToArray(strData, strDelimiter) {
	var strDelimiter = (strDelimiter || ",");
	var objPattern = new RegExp(
			("(\\" + strDelimiter + "|\\r?\\n|\\r|^)"
					+ "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" + "([^\"\\"
					+ strDelimiter + "\\r\\n]*))"), "gi");
	var arrData = [ [] ];
	var arrMatches = null;
	while (arrMatches = objPattern.exec(strData)) {
		var strMatchedDelimiter = arrMatches[1];
		if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
			arrData.push([]);
		}
		var strMatchedValue;
		if (arrMatches[2]) {

			strMatchedValue = arrMatches[2].replace(new RegExp("\"\"", "g"),"\"");
		} else {
			strMatchedValue = arrMatches[3];
		}
		arrData[arrData.length - 1].push(strMatchedValue);
	}
	return arrData;
}

// store value csv to json
Selenium.prototype.doStoreValueCSVToJson = function(locator, variableName) {
	saveVariable(variableName, csvToJSON(readTextFile(locator)));
}
// store value csv to array
Selenium.prototype.doStoreValueCSVToArray = function(locator, variableName) {
	saveVariable(variableName, csvToArray(locator));
}

// get row length in csv file
Selenium.prototype.doStoreRowLengthCSV = function(locator, variableName) {
	arrayCVS = csvToArray(locator);
	saveVariable(variableName, arrayCVS.length);
}

// //get collumn lengt in csv file
Selenium.prototype.doStoreColLengthCSV = function(locator, variableName) {
	arrayCVS = csvToArray(locator);
	colCSV = arrayCVS[0];
	saveVariable(variableName, colCSV.length);
}

Selenium.prototype.doStoreResponHeader = function(callback) {
	var request = new XMLHttpRequest();
	request.open("GET", callback, true);
	request.send();
	request.onreadystatechange = function() {
		if (this.readyState == this.HEADERS_RECEIVED) {
			LOG.info(request.getAllResponseHeaders());
		}
	}
}

// ==================== Get from databases ================================

function openRequest(locator, attribute){
	var xhttp = new XMLHttpRequest();
	var result = "";
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			result = this.responseText;			
			//saveVariable(attribute, result);
		}
	};
	xhttp.open("POST", base_url + "api/getData", false);
	xhttp.send(locator);
	return result;
}


Selenium.prototype.getDataSQL = function(locator, attribute) {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText;			
			saveVariable(attribute, result);
		}
	};
	xhttp.open("POST", base_url + "api/getData", false);
	xhttp.send(locator);
	return true;
	
};

Selenium.prototype.getDataSQLtoArray = function(locator, attribute) {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText;			
			saveVariable(attribute, jsonToArray(result));
		}
	};
	xhttp.open("POST", base_url + "api/getData", false);
	xhttp.send(locator);
	return true;
};

Selenium.prototype.getDataSQLtoJson = function(locator, attribute) {

	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText;			
			saveVariable(attribute, stringToJson(result));
		}
	};
	xhttp.open("POST", base_url + "api/getData", false);
	xhttp.send(locator);
	return true;
};


function stringToJson(locator){
	return JSON.parse(locator);
}
function getLengthJson(locator){
	return Object.keys(locator).length;
}

Selenium.prototype.doStoreKeysJson = function(locator, variableName) {
	LOG.info(locator[1]);
	saveVariable(variableName, Object.keys(locator));	
}

Selenium.prototype.doStoreStringToJson = function(locator, variableName) {
	LOG.info(locator);
	saveVariable(variableName, stringToJson(locator));
}

Selenium.prototype.doStoreLengthJson = function(locator, variableName) {
	
	var jsonObject = stringToJson(locator);
	saveVariable(variableName, getLengthJson(jsonObject));
}

Selenium.prototype.doStoreLengthHeaderJson = function(locator, variableName) {
	
	var jsonObject = stringToJson(locator);
	var length = getLengthJson(jsonObject);
	saveVariable(variableName, Object.keys(jsonObject[0]).length);
	
}

Selenium.prototype.doConvertJsonToArray = function(locator, variableName) {
	
	saveVariable(variableName, jsonToArray(locator));
}

Selenium.prototype.doRandomString = function(locator, variableName) {
	var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for(var i = 0; i < locator; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
	saveVariable(variableName, text);
	
}

Selenium.prototype.doRandomNumber = function(locator, variableName) {
	var text = "";
    var possible = "0123456789";
    for(var i = 0; i < locator; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
	saveVariable(variableName, text);
}


Selenium.prototype.doKickUser = function(locator,value) {

	var query = "";
	var modul = value;
	
LOG.info("start, "+value);
	if (value.toLowerCase()=="bo"){
		query = value + ";DELETE FROM idm_user_login_flag WHERE user_id='"+locator+"'";
	} else if (value.toLowerCase()==="fo"){
		query = value + ";DELETE FROM idm_user_login_flag WHERE user_id IN(select user_id from idm_user where nm='"+locator+"')";
	}
	LOG.info(query);
	LOG.info("finis");
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			
			var result = this.responseText;
			saveVariable(value, result);
		}
	};
	xhttp.open("POST", base_url + "api/executeUpdate", false);
	xhttp.send(query);
}

function saveVariable(varName, varValue){
	storedVars[varName]=varValue;
	
	if(typeof valueBucket!=='undefined'){
		valueBucket[varName]=varValue;
	}
}

function jsonToArray(locator){
	var jsonObject = stringToJson(locator);
	var length = getLengthJson(jsonObject);
	var header = Object.keys(jsonObject[0]);
	var arrData = [];
	arrData.push(header);
	for (var i=0 ; i<length;i++){
		var rowValue = [];
		for(var j=0;j<header.length;j++){
			rowValue.push(jsonObject[i][header[j]]);
		}
		arrData.push(rowValue);
		
	}
	return arrData;
}

Selenium.prototype.doSetAttribute = function(locator,value) {
	var attr = value.split("=");
	
	var element = selenium.browserbot.findElement(locator);
	element.setAttribute(attr[0], attr[1]); 
};

//=============================SIKULI X COMMAND=========================================
Selenium.prototype.doSikuli = function(locator, attribute) {
	var err = false;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var result = this.responseText;	
			err=false;
			LOG.info(result);
		}
		else if(this.status == 500){
			err=true;
			LOG.info("ERROR");
		}
	};
	
	var request= '{"locator":"'+locator+'","attribute":"'+attribute+'"}';
	xhttp.open("POST", base_url + "sikuli/doSomething", false);
	xhttp.setRequestHeader('Content-type','application/json');
	xhttp.send(request);
	
	if(err){
		return true;
	}
};


//===============================Call Service==================================================
Selenium.prototype.doCallService = function(locator, attribute) {
	storedVars["t_result"]=null;
	var result="";
	var xmlhttp = new XMLHttpRequest();
            xmlhttp.open('POST', storedVars["wsdlURL"]+'/services', true);

            var sr ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servlet.jetty.openqa.org/"><soapenv:Header/><soapenv:Body><ser:getTerranServices><arg0>'+locator+'</arg0><arg1>'+attribute+'</arg1></ser:getTerranServices></soapenv:Body></soapenv:Envelope>';

            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status == 200) {
						var xml=xmlhttp.responseXML;
						var xmlString= xmlhttp.responseText;
						var temp = xml.getElementsByTagName("result1")[0].textContent;
						
						result =result+temp;
						if(result=="null"){
							LOG.error("Service doesn't return any information caused by exception, please check with the QAE :"+result);
						}else{
							var json = result,
							obj = JSON.parse(json);
							for (var key in obj){
								storedVars[""+key]=obj[key];
							}
						}
						
                    }
                }
            }
            xmlhttp.setRequestHeader('Content-Type', 'text/xml');
            xmlhttp.send(sr);
			return Selenium.prototype.doPause.call(this,10000);
			
};
