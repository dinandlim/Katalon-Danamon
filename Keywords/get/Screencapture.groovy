package get

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import javax.imageio.ImageIO

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import ru.yandex.qatools.ashot.*
import ru.yandex.qatools.ashot.shooting.ShootingStrategies

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

public class Screencapture {
	
	KeywordLogger log = new KeywordLogger();
	WebDriver driver = DriverFactory.getWebDriver();
	
	@Keyword
	public String getElement(TestObject object, String fileName) {

		WebElement element = WebUiCommonHelper.findWebElement(object, 30);
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver,element);
		ImageIO.write(screenshot.getImage(), "JPG", new File(getImgPath(fileName)))
		log.logPassed("Taking screenshot successfully\n[[ATTACHMENT|"+getImgPath(fileName)+"]]")
		return "Hello World"
	}

	@Keyword
	public void getEntirePage(String fileName) {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);
		ImageIO.write(screenshot.getImage(), "JPG", new File(getImgPath(fileName)))
		log.logPassed("Taking screenshot successfully\n[[ATTACHMENT|"+getImgPath(fileName)+"]]")
	}
	
	private String getImgPath(String fileName){
		
		
		File theDir = new File(System.getProperty("user.dir")+"/ScreenCapture/");
		
		// if the directory does not exist, create it
		if (!theDir.exists()) {
				theDir.mkdir();
		}
		return theDir.getPath()+File.separator+fileName;
	}
	
	
	
}
