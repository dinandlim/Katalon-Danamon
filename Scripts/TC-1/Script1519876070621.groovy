import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Button Language English'))

WebUI.setText(findTestObject('English/Text Field Company ID'), CompanyId)

WebUI.setText(findTestObject('English/Text Field User Id'), UserId)

WebUI.setText(findTestObject('English/Text Field Password'), Password)

WebUI.click(findTestObject('English/Button Login'))

WebUI.waitForElementPresent(findTestObject('English/Button Logout'), 0)

WebUI.takeScreenshot('D:\\Screenshot\\pic.png')

WebUI.click(findTestObject('English/Button Logout'))

WebUI.waitForElementPresent(findTestObject('English/Button Relogin'), 0)

WebUI.click(findTestObject('English/Button Relogin'))

WebUI.waitForElementPresent(findTestObject('English/Text Field Company ID'), 0)

WebUI.setText(findTestObject('English/Text Field Company ID'), CompanyId)

WebUI.setText(findTestObject('English/Text Field User Id'), UserId)

WebUI.setText(findTestObject('English/Text Field Password'), Password)

WebUI.click(findTestObject('English/Button Login'))

WebUI.click(findTestObject('English/Button Logout'))

